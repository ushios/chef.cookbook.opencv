#
# Cookbook Name:: opencv
# Recipe:: default
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'opencv::install_opencv_2_4_5'
