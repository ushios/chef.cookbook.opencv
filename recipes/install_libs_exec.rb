#
# Cookbook Name:: opencv
# Recipe:: install_libs_exec
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

package 'python' do
	action :install
end

package 'autoconf' do
	action :install
end

package 'libtbb2 libtbb-dev' do
	action :install
end

package 'libeigen2-dev' do
	action :install
end

package 'cmake' do
	action :install
end

package 'openexr' do
	action :install
end

package 'gstreamer-plugins-*' do
	action :install
end

package 'freeglut3-dev' do
	action :install
end

package 'libglui-dev' do
	action :install
end

package 'libavc1394-dev libdc1394-22-dev libdc1394-utils' do
	action :install
end
