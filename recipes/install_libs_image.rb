#
# Cookbook Name:: opencv
# Recipe:: install_libs_image
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#
package 'libjpeg-dev' do
	action :install
end

package 'libopenjpeg-dev' do
	action :install
end

package 'jasper' do
	action :install
end

package 'libjasper-dev libjasper-runtime' do
	action :install
end

package 'libpng12-dev' do
	action :install
end

package 'libpng++-dev libpng3' do
	action :install
end

package 'libpnglite-dev' do # libpngwriter0-dev libpngwriter0c2' do
	action :install
end

package 'libtiff4-dev libtiff-tools pngtools' do
	action :install
end

package 'zlib1g-dev zlib1g-dbg' do
	action :install
end

package 'v4l2ucp' do
	action :install
end






