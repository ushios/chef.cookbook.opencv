#
# Cookbook Name:: opencv
# Recipe:: install_libs_exec
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

package 'libxine-dev' do
	action :install
end

package 'libxvidcore-dev' do
	action :install
end

package 'libva-dev' do
	action :install
end

package 'libssl-dev' do
	action :install
end

package 'libv4l-dev' do
	action :install
end

package 'libvo-aacenc-dev' do
	action :install
end

package 'libvo-amrwbenc-dev' do
	action :install
end

# version missing.
#package 'liborbis-dev' do
#	action :install
#end

package 'libvpx-dev' do
	action :install
end

package 'ffmpeg' do
	action :install
end
