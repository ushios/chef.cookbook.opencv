#
# Cookbook Name:: opencv
# Recipe:: install_opencv
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

package 'libopencv-dev' do
	action :install
end
