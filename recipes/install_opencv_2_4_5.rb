#
# Cookbook Name:: opencv
# Recipe:: install_opencv_2_4_5
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

dir='/var/chef/opencv/opencv2.4.5'

directory dir do
	action :create
	recursive true
	mode 0755
	owner 'root'
	group 'root'
end

package 'build-essential' do
	action :install
end

execute 'build-dep opencv' do
	command "sudo apt-get -yV build-dep opencv && touch #{dir}/.build-dep-opencv"
	creates "#{dir}/.build-dep-opencv"
	user 'root'
	action :run
end

package 'opencl-headers' do
	action :install
end

include_recipe 'opencv::install_libs_image'
include_recipe 'opencv::install_libs_exec'
include_recipe 'opencv::install_libs_movie'

tar="/usr/local/#{node[:opencv][:name_v2_4_5]}.tar.gz"
untar="/usr/local/#{node[:opencv][:name_v2_4_5]}"
creates_file='/usr/local/lib/libopencv_core.so.2.4.5'

remote_file tar do
	source node[:opencv][:tar_v2_4_5]
	action :create_if_missing
	mode 0644
	owner 'root'
	group 'root'
end

execute 'untar' do
	command "tar xf #{tar}"
	creates "#{untar}"
	cwd '/usr/local'
	user 'root'
end

execute 'ccmake' do
	command "cmake -DBUILD_DOCS=ON -DBUILD_EXAMPLES=ON -DCMAKE_BUILD_TYPE=RELEASE -DWITH_TBB=ON -DINSTALL_C_EXAMPLES=ON -DWITH_OPENCL=OFF -DWITH_CUDA=OFF -DWITH_OPENNI=ON -DWITH_UNICAP=ON -DWITH_V4L=ON -DWITH_XINE=ON  ."
	cwd untar
	creates creates_file
	user 'root'
end

execute 'make' do
	command 'make'
	cwd untar
	creates creates_file
	user 'root'
end

execute 'make install' do
	command 'make install'
	cwd untar
	creates creates_file
	user 'root'
end

execute 'ldconfig' do
	command 'ldconfig'
	cwd untar
	user 'root'
end
