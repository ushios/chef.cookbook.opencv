#
# Cookbook Name:: opencv
# Recipe:: php_facedetect
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

dir = '/var/chef/opencv'
directory dir do
	recursive true
	action :create
	owner 'root'
	group 'root'
	mode 0744
end	

package 'git' do
	action :install
end

facedetect_dir = "#{dir}/php_facedetect"
git facedetect_dir do
	repository node[:opencv_php_facedetect][:repository]
	reference node[:opencv_php_facedetect][:reference]
	action :sync
end

#execute 'make facedetect' do
#	command 'phpize && ./configure && make && make install'
#	cwd facedetect_dir
#	action :run
#	user 'root'
#end
