#
# Cookbook Name:: opencv
# Recipe:: python
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

package 'python-opencv' do
	action :install
end
